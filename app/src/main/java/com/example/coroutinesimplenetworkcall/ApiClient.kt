package com.example.coroutinesimplenetworkcall

import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class ApiClient {

    companion object{

        var retrofit : Retrofit? = null
        val TIME_OUT_LIMIT : Long = 180
        val BASE_URL = "http://ipinfo.io/"

        @Synchronized
        fun getInstance() : Retrofit? = when(retrofit){
            null -> {
                val gson = GsonBuilder().setLenient().create()

                val client = OkHttpClient.Builder().connectTimeout(TIME_OUT_LIMIT, TimeUnit.SECONDS)
                    .writeTimeout(TIME_OUT_LIMIT, TimeUnit.SECONDS)
                    .readTimeout(TIME_OUT_LIMIT, TimeUnit.SECONDS)
                    .build()

                retrofit =  Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(CoroutineCallAdapterFactory())
                    .build()

                retrofit
            }
            else -> retrofit
        }
    }
}