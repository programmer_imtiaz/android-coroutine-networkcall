package com.example.coroutinesimplenetworkcall

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Url


interface ApiService {

    @GET("json")
    suspend fun getIPDetails(): Response<IPResponse?>?
}