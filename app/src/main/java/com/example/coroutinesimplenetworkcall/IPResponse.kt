package com.example.coroutinesimplenetworkcall

import com.google.gson.annotations.SerializedName

data class IPResponse(

	@field:SerializedName("country")
	val country: String? = null,

	@field:SerializedName("loc")
	val loc: String? = null,

	@field:SerializedName("city")
	val city: String? = null,

	@field:SerializedName("org")
	val org: String? = null,

	@field:SerializedName("timezone")
	val timezone: String? = null,

	@field:SerializedName("ip")
	val ip: String? = null,

	@field:SerializedName("postal")
	val postal: String? = null,

	@field:SerializedName("readme")
	val readme: String? = null,

	@field:SerializedName("region")
	val region: String? = null
)