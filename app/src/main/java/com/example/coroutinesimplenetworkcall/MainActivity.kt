package com.example.coroutinesimplenetworkcall

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response
import java.lang.Exception

class MainActivity : AppCompatActivity() {

    var apiService : ApiService? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        button.setOnClickListener{
            progressBar.visibility = View.VISIBLE
            getIPResponse()
        }

    }

    private fun getIPResponse(){
        CoroutineScope(Dispatchers.IO).launch {
            apiService = ApiClient.getInstance()?.create(ApiService::class.java)
            apiService?.let {
                try {
                    val response : Response<IPResponse?>? = it.getIPDetails()
                    if(response != null && response.isSuccessful){
                        Log.e("IpResponseSuccess", response.code().toString())
                        updateView(response.body())
                    }
                }
                catch (ex : Exception){
                    Log.e("IpResponseExp", ex.toString())
                    updateView(null)
                }
            }
        }
    }

    private fun updateView(ipResponse: IPResponse?){
        CoroutineScope(Dispatchers.Main).launch{
            progressBar.visibility = View.GONE
            ipResponse?.let {
                textViewIp.text = it.ip ?: ""
                textViewCountry.text = it.country ?: ""
                textViewLocation.text = it.loc ?: ""
            }
        }
    }
}
